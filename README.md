# vCenter SSO OpenLDAP SASL

An OpenLDAP for vCenter Single Sign On uses Pass-through authentication with SASL

## Scenario

The lab has a vCenter and configure it to use SSO. I want to maintain the users 
only but do not want to maintain the password. The good news is our company has 
a LDAP backend and it can be used for authentication. The bad news is it has its 
own schema.

So there is an OpenLDAP instance configured as a SSO identity source in vCenter 
and delegate authentication by SASL to the LDAP backend.  

```mermaid
graph LR
  subgraph OpenLDAP
B --> |SASL| C[saslauthd]
  end
  subgraph Backend
C --> |LDAP| D[LDAP backend]
  end
  subgraph vCenter
A --> |auth with identity source| B[slapd]
  end
  subgraph Client
S[Web Browser] --> |Login| A[vCenter SSO]
  end
```

## References

There are many information from Internet.  

* [Pass-Trough authentication with SASL](https://ltb-project.org/documentation/general/sasl_delegation)
* [OpenLDAP Pass through authentication with SASL to Active Directory](https://www.surekhatech.com/blog/openldap-pass-through-authentication-with-sasl-to-active-directory)
* old - [SASL Pass-Through Authentication with OpenLDAP](https://lazy-sysadmin.tumblr.com/post/11985887802/sasl-pass-through-authentication-with-openldap)
* [OpenLDAP schemas supported in VMware vCenter Single Sign-On (2064977)](https://kb.vmware.com/s/article/2064977)
* [Cyrus SASL](https://www.cyrusimap.org/sasl/)

